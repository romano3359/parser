var customRequest = require("./request");
var customMysql = require("./mysql");
var customFile = require("./file");

customMysql.getPeriod(5300, 31680)
	.then(function(data) {
		var cnt = 0;

		var timerId = setTimeout(function tick() {
			if(cnt >= data.length) {
				//customMysql.closeConnection();
				return;
			}
			
			console.log(data[cnt].ID);
			customRequest.parseReviews(data[cnt]);

			cnt++;
			timerId = setTimeout(tick, 500);
		}, 500);
	}).catch(function(err) {
		console.log(err);
	});