const http = require('http');
const https = require('https');
const fs = require('fs');
const uploadDirPhone = "/path-to-photo/";

function getFilePhone(url, id, cnt) {
	var file = fs.createWriteStream(uploadDirPhone + id + "_" + cnt + ".jpg");

	/*var request = http.get(
		url, 
		function(response) {
			response.pipe(file);
		}
	);*/

	var request = https.get(
		url, 
		function(response) {
			response.pipe(file);
		}
	);
}

exports.getFilePhone = getFilePhone;
