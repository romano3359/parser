const request = require('request');
const cheerio = require('cheerio');
const Promise = require('module-promise');

var customMysql = require("./mysql");
var customFile = require("./file");

var Iconv = require('iconv').Iconv;
var fromEnc = 'cp1251';
var toEnc = 'utf-8';
var translator = new Iconv(fromEnc, toEnc);

// STEP 6
function parseReviews(inputData) {
	return new Promise(function(resolve, reject){
		request(
			{
				uri: inputData.URL, 
				method:'GET', 
				encoding:null
			},
		    function (err, res, page) {
		    	page = translator.convert(page).toString();
		        var $ = cheerio.load(page);
		        var data = {};
		        
		        try {
		        	var table = $("div.archive > table[width^=100] td[valign=top]").eq(1);
		        	var time = table.find("div span").text();

		        	table.find("div").remove();
		        	var address = table.text();

		        	var phone = [];
		        	var src = '';
		        	$("div.archive > table[width^=100]").find("div img").each(function(j) {
		        		phone[j] = $(this).parent("div").text();
		        		src = $(this).attr('src').replace(/\/\//g, "https://");
		        		if(src.length > 0)
		        			customFile.getFilePhone(src, inputData.ID, j);
		        	});

		        	var text = $("div.archive > div[style^=background]").eq(0).html();

		        	if(text != null)
		        		text = text.replace(/'/g, "\\'").replace(/"/g, '\\"');
		        	else
		        		text = '';

		        	data = {
		        		"ID": inputData.ID,
		        		"ADDRESS": address,
		        		"TIME": time,
		        		"PHONE": phone,
		        		"TEXT": text
		        	}

		        	customMysql.update("ORGANIZATION", data, inputData.ID)
		        		.then(function(data) {

		        		})
		        		.catch(function(err) {

		        		});

		        	var arUrl = inputData.URL.split("/");
		        	var reviewUrl = "https://" + arUrl[2] + "/otzyvy/" + arUrl.pop();

		        	parseReviews2(inputData, reviewUrl)
			        	.then(function(data) {

			        	})
			        	.catch(function(err) {
			        		console.log(err);
			        	});

		        	resolve(data);
		        }
		        catch(err) {
		        	reject(err);
		        }
		    }
		);
	});
}

// STEP 6
function parseReviews2(inputData, reviewUrl) {
	return new Promise(function(resolve, reject){
		request(
			{
				uri: reviewUrl, 
				method:'GET', 
				encoding:null
			},
		    function (err, res, page) {
		    	page = translator.convert(page).toString();
		        var $ = cheerio.load(page);
		        var data = {};
		        
		        try {
		        	$("table.archive[width=600]").eq(1).find("div:not(.line)").each(function(i) {
		        		var name = $(this).find("a").text();
		        		var arInfo = $(this).find("a").attr("title").split(".");
		        		var date = arInfo[0];
		        		var author = arInfo[1];

		        		var grade;
		        		var gradeImg = $(this).next("img").attr('src').split("/").pop();
		        		if(gradeImg == "signgood.png")
		        			grade = 1;
		        		else
		        			grade = 0;

		        		var text = $(this).next("img").next("span").html()/*.replace(/'/g, "\\'").replace(/"/g, '\\"')*/;
		        		if(text != null)
			        		text = text.replace(/'/g, "\\'").replace(/"/g, '\\"');
			        	else
			        		text = "";

		        		var is_full;
		        		var link_full;
		        		if($(this).next("span").find("a").length > 0) {
		        			is_full = 1;
		        			link_full = $(this).next("span").find("a").attr("href");
		        		}
		        		else {
		        			is_full = 0;
		        			link_full = "";
		        		}

		        		data = {
		        			"GRADE": grade,
		        			"NAME": name,
		        			"AUTHOR": author,
		        			"TEXT": text,
		        			"DATE": date,
		        			"ORG_ID": inputData.ID,
		        			"SUBCAT_ID": inputData.SUBCATEGORY_ID,
		        			"IS_FULL": is_full,
		        			"LINK_FULL": link_full
		        		};

		        		customMysql.insert("REVIEWS", data)
			        		.then(function(data) {

			        		})
			        		.catch(function(err) {
			        			console.log(err);
			        		});
		        	});

		        	resolve(data);
		        }
		        catch(err) {
		        	reject(err);
		        }
		    }
		);
	});
}

exports.parseReviews = parseReviews;