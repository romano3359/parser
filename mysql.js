const mysql = require("mysql");
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "user",
    database: "parser",
    charset: "UTF8_UNICODE_CI"
});
const Promise = require('module-promise');

var customRequest = require("./request");
var customFile = require("./file");
var importData;

// STEP 6
function getPeriod(min, max) {
	return new Promise(function(resolve, reject) {
		connection.query(
			"SELECT * FROM `ORGANIZATION` WHERE `IS_REVIEW` = 1 AND `ID` >= " + min + " AND `ID` < " + max, 
			function(err, rows, fields) {
				if(err) {
					reject(err);
				}
				else {
					var data = [];
					for(var i = 0; i < rows.length; i++) {
						data[i] = {
							"ID": rows[i].ID,
							"URL": rows[i].URL,
							"SUBCATEGORY_ID": rows[i].SUBCATEGORY_ID
						}
					}
					resolve(data);
				}
			}
		);
	});
}

function insert(table, data) {
	return new Promise(function(resolve, reject) {
		var strCol = '';
		var strVal = '';
		for(var key in data) {
			if(key != "ID") {
				strCol = strCol + "`" + key + "`, ";
				strVal = strVal + "'" + data[key] + "', ";
			}
		}

		strCol = "(" + strCol.substr(0, strCol.length - 2) + ")";
		strVal = "(" + strVal.substr(0, strVal.length - 2) + ")";

		if(strCol.length > 0 && strVal.length > 0)
			connection.query(
				"INSERT INTO `" + table + "` " + strCol + " VALUES " + strVal, 
				function(err, rows, fields) {
					if(err) {
						reject(err);
					}
					else {
						resolve(data);
					}
				}
			);
		else
			reject("empty data in query");
	});
}

function update(table, data, id) {
	return new Promise(function(resolve, reject) {
		var str = '';
		for(var key in data) {
			if(key != "ID")
				str = str + "`" + key + "` = '" + data[key] + "', ";
		}

		str = str.substr(0, str.length - 2);

		if(str.length > 0)
			connection.query(
				"UPDATE `" + table + "` SET " + str + " WHERE `ID` = " + id, 
				function(err, rows, fields) {
					if(err) {
						reject(err);
					}
					else {
						resolve(data);
					}
				}
			);
		else
			reject("empty data in query");
	});
}

function closeConnection() {
	connection.end();
}

exports.getPeriod = getPeriod;
exports.insert = insert;
exports.update = update;
exports.closeConnection = closeConnection;